package com.customer_management.customer_management;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.customer_management.customer_management.view.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ratePassangerActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {
    GoogleMap map;
    ImageView ivProfileUser;
    RelativeLayout rlLayoutBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_passanger);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Init();
    }

    private void Init() {
        rlLayoutBottom = findViewById(R.id.rlLayoutBottom);
        ivProfileUser = findViewById(R.id.ivProfileUser);

        ivProfileUser.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(0, 0))
                .title("Marker"));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivProfileUser:
                startActivity(new Intent(this, walletActivity.class));
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}