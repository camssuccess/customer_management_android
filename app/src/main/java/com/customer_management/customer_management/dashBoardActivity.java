package com.customer_management.customer_management;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.customer_management.customer_management.view.BaseActivity;

public class dashBoardActivity extends BaseActivity {
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

    relativeLayout=findViewById(R.id.sumLayout);
    relativeLayout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(dashBoardActivity.this, rideNowActivity.class));
        }
    });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
