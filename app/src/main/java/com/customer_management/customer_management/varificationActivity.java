package com.customer_management.customer_management;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.customer_management.customer_management.view.BaseActivity;

public class varificationActivity extends BaseActivity implements View.OnClickListener {
    public int counter;
    Button btnText;
    TextView tvResendCode, tvTimer;
    CountDownTimer countDownTimer;
    EditText etOtp;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification);

      /*  new CountDownTimer(50000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvResendCode.setText(String.valueOf(counter));
                counter++;
            }
            @Override
            public void onFinish() {
                tvResendCode.setText("Finished");
            }
        }.start();*/

        resendCode();
        initillization();
        init();
    }

    private void initillization() {
        btnText = findViewById(R.id.btnText);
        etOtp = findViewById(R.id.etOtp);
        tvTimer = findViewById(R.id.tvTimer);
        btnText.setOnClickListener(this);
        tvTimer.setOnClickListener(this);
        //getText Via String
        otp = etOtp.getText().toString().trim();
    }

    private void init() {
        // btnVerify = rootView.findViewById(R.id.btnVerify);
        tvResendCode = findViewById(R.id.tvResendCode);
        etOtp = findViewById(R.id.etOtp);

        onClickListener();
    }


    private void resendCode() {

        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResendCode.setVisibility(View.GONE);
                tvTimer.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tvTimer.setVisibility(View.GONE);
                tvResendCode.setVisibility(View.VISIBLE);
            }
        }.start();

    }

    private void onClickListener() {
        tvResendCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnText:
                startActivity(new Intent(this, varifyActivity.class));


                break;

            case R.id.tvResendCode:
                resendCode();

                break;
            case R.id.tvTimer:


                break;

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}