package com.customer_management.customer_management;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.customer_management.customer_management.view.BaseActivity;

public class rideLaterConfirmationActivity extends BaseActivity implements View.OnClickListener {
    ImageView ivback;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_later_confirmation_activity);
        initiallization();
    }

    private void initiallization() {
        ivback=findViewById(R.id.ivback);
        ivback.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivback:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
