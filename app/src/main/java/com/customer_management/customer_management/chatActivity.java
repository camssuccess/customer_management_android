package com.customer_management.customer_management;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.MotionEvent;
import android.view.View;

import android.view.inputmethod.InputMethodManager;

import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import com.customer_management.customer_management.view.BaseActivity;



public class chatActivity extends BaseActivity implements View.OnClickListener {
    ImageView ivback, ivSend;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        Initiallization();

    }

    private void Initiallization() {
       ivback = findViewById(R.id.ivback);
       ivSend = findViewById(R.id.ivSend);
       ivback.setOnClickListener(this);
       ivSend.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                onBackPressed();
                break;
                case R.id.ivSend:
                startActivity(new Intent(this,callingActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();

    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);

    }
}
