package com.customer_management.customer_management;

import android.content.Intent;
import android.os.Bundle;

import com.customer_management.customer_management.view.AppSettings;
import com.customer_management.customer_management.view.BaseActivity;

public class rootClass extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(AppSettings.getString(AppSettings.userName).equals("user123") && AppSettings.getString(AppSettings.password).equals("123456")){

            moveToDashBoard();
        }else{
            moveToLogin();
        }

    }

    private void moveToDashBoard() {
        startActivity(new Intent(mActivity,dashBoardActivity.class));
    }

    private void moveToLogin() {
        startActivity(new Intent(mActivity,loginActivity.class));
    }

}
