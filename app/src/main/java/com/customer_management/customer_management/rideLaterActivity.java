package com.customer_management.customer_management;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.customer_management.customer_management.view.BaseActivity;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class rideLaterActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView recyclerProfile, recyclerData;
    LinearLayoutManager llManagerProfile, llManagerData;
    ProfileAdapter profileAdapter;
    DataAdapter dataAdapter;
    LinearLayout llSpinner;
    DatePickerDialog picker;
    TextView tvv;

    String TAG = "rideLaterActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_later_activity);

        findViewById();
    }

    private void findViewById() {
        recyclerProfile = findViewById(R.id.recyclerProfile);
        recyclerData = findViewById(R.id.recyclerData);
        llSpinner = findViewById(R.id.llSpinner);
        tvv = findViewById(R.id.tvv);

        llManagerProfile = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerProfile.setLayoutManager(llManagerProfile);

        llManagerData = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerData.setLayoutManager(llManagerData);

        dataAdapter = new DataAdapter();
        recyclerData.setAdapter(dataAdapter);
        recyclerData.setHasFixedSize(true);

        profileAdapter = new ProfileAdapter();
        recyclerProfile.setAdapter(profileAdapter);
        recyclerProfile.setHasFixedSize(true);
        llSpinner.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSpinner:
                setNormalPicker();
                break;
        }
    }

    public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

     /*   public DataAdapter(Activity activty) {
            //this.data = favlist;
            this.context=activty;
        }*/


        @Override
        public DataAdapter.DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.data_layout, parent, false);

            return new DataAdapter.DataHolder(view);
        }

        @Override
        public int getItemCount() {

            return 3;
        }

        @Override
        public void onBindViewHolder(final DataAdapter.DataHolder holder, final int position) {

        }


        public class DataHolder extends RecyclerView.ViewHolder {


            public DataHolder(View itemView) {
                super(itemView);

            }

        }
    }

    private void setNormalPicker() {

      /*  final Calendar today = Calendar.getInstance();
        findViewById(R.id.imgButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(mActivity, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        Log.d(TAG, "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);
                        Toast.makeText(mActivity, "Date set with month" + selectedMonth + " year " + selectedYear, Toast.LENGTH_SHORT).show();
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

                builder.setActivatedMonth(Calendar.JULY)
                        .setMinYear(1990)
                        .setActivatedYear(2017)
                        .setMaxYear(2030)
                        .setMinMonth(Calendar.FEBRUARY)
                        .setTitle("Select trading month")
                        .setMonthRange(Calendar.FEBRUARY, Calendar.NOVEMBER)
                        // .setMaxMonth(Calendar.OCTOBER)
                        // .setYearRange(1890, 1890)
                        // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                        //.showMonthOnly()
                        // .showYearOnly()
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                                Log.d(TAG, "Selected month : " + selectedMonth);
                                // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                                Log.d(TAG, "Selected year : " + selectedYear);
                                // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .build()
                        .show();

            }
        });*/

        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        tvv.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        Toast.makeText(rideLaterActivity.this, "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year, Toast.LENGTH_LONG).show();
                    }
                }, year, month, day);
        picker.show();



    }

    /*    findViewById(R.id.date_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(walletActivity.this, null, 2017,
                        cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
                dialog.show();
            }
        });*/
    public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.profileHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

     /*   public DataAdapter(Activity activty) {
            //this.data = favlist;
            this.context=activty;
        }*/


        @Override
        public ProfileAdapter.profileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.layout_profile, parent, false);

            return new ProfileAdapter.profileHolder(view);
        }

        @Override
        public int getItemCount() {

            return 3;
        }

        @Override
        public void onBindViewHolder(final ProfileAdapter.profileHolder holder, final int position) {

            holder.llfirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(rideLaterActivity.this, bookingActivity.class));
                }
            });
        /*    holder.text_username.setText("Jessica Lim");

            holder.text_wallet.setText("RM 4");
            holder.txt_currency.setText("RM 8");
            holder.LinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(RideLaterActiity.this,Chat.class));
                }
            });*/
        }


        public class profileHolder extends RecyclerView.ViewHolder {

          LinearLayout llfirst;

            public profileHolder(View itemView) {
                super(itemView);

                //CustomImageview
                llfirst=itemView.findViewById(R.id.llfirst);

            }

        }
    }

}
