package com.customer_management.customer_management;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.customer_management.customer_management.view.BaseActivity;

public class bookingConfirmedActivity extends BaseActivity implements View.OnClickListener {
    Button btnGoBackToLocation;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_confirmed_activity);
        initillization();
    }

    private void initillization() {
        btnGoBackToLocation=findViewById(R.id.btnGoBackToLocation);
        btnGoBackToLocation.setOnClickListener(this);
    }

    private void AlertLogoutPopUp() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(this).inflate(R.layout.booking_confirmation_popup, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        ImageView ivProfile = alertDialog.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(bookingConfirmedActivity.this, passangerActivity.class));
            }
        });

       /*  Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.booking_confirmation_popup);
        Window window = dialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);

        dialog.show();
   //     dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);
       */
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnGoBackToLocation:
                AlertLogoutPopUp();
                break;
        }
    }
}
