package com.customer_management.customer_management;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.customer_management.customer_management.view.BaseActivity;

public class rideLaterCarSelectionActivtiy extends BaseActivity implements View.OnClickListener {
ImageView ivLeft,ivRight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_class);
        //setContentView(R.layout.ride_later_car_selection);
        Initiallization();

    }

    private void Initiallization() {
        ivLeft =findViewById(R.id.ivLeft);
        ivRight=findViewById(R.id.ivRight);

        //OnClickListener
        ivLeft.setOnClickListener(this);
       ivRight.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
             case R.id.ivLeft:
                onBackPressed();
                break;
             case R.id.ivRight:
                startActivity(new Intent(this, rideLaterConfirmationActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
