package com.customer_management.customer_management;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.customer_management.customer_management.view.BaseActivity;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class walletActivity extends BaseActivity implements View.OnClickListener {
    WalletAdapter walletAdapter;
    RecyclerView recyclerView;
    TextView tvRight,tvLeft;
    LinearLayoutManager llManager;
    RelativeLayout relYear,relmonth;
    int choosenYear=2020;
    String TAG = "walletActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_activity);
        Initillization();
        //setNormalPicker();
    }

    private void Initillization() {
        recyclerView = findViewById(R.id.recyclerView_wallet);
        llManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llManager);
        walletAdapter = new WalletAdapter();
        recyclerView.setAdapter(walletAdapter);
        recyclerView.setHasFixedSize(true);
        relYear=findViewById(R.id.relYear);
        tvRight=findViewById(R.id.tvRight);
        tvLeft=findViewById(R.id.tvLeft);
        relmonth=findViewById(R.id.relMonth);
        relYear.setOnClickListener(this);
        relmonth.setOnClickListener(this);
    }

    @Override
      public void onClick(View view) {
        switch (view.getId()){
            case R.id.relYear:
                chooseYearOnly();
                //createDialogWithoutDateField().show();
                break;
            case R.id.relMonth:
                chooseMonthOnly();
               // createDialogWithoutDateField().show();

        }
    }



    private DatePickerDialog createDialogWithoutDateField() {
        DatePickerDialog dpd = new DatePickerDialog(this, null, 2014, 1, 24);
        try {
            java.lang.reflect.Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
            for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField.get(dpd);
                    java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
                    for (java.lang.reflect.Field datePickerField : datePickerFields) {
                        Log.i("test", datePickerField.getName());
                        if ("mDaySpinner".equals(datePickerField.getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);

                        }
                    }
                }
            }
        }
        catch (Exception ex) {
        }
        return dpd;
    }



    public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.WalletHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

     /*   public WalletAdapter(Activity activty) {
            //this.data = favlist;
            this.context=activty;
        }*/


        @Override
        public WalletAdapter.WalletHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.wallet_adapter, parent, false);

            return new WalletAdapter.WalletHolder(view);
        }

        @Override
        public int getItemCount() {

            return 4;
        }

        @Override
        public void onBindViewHolder(final WalletAdapter.WalletHolder holder, final int position) {

            holder.text_username.setText("Jessica Lim");

            holder.text_wallet.setText("RM 4");
            holder.txt_currency.setText("RM 8");
            holder.LinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  startActivity(new Intent(walletActivity.this, chatActivity.class));
                }
            });
        }


        public class WalletHolder extends RecyclerView.ViewHolder {

              ImageView imageView;
              TextView text_wallet,text_username,txt_currency;
              LinearLayout LinearLayout;

            public WalletHolder(View itemView) {
                super(itemView);
                imageView    =itemView.findViewById(R.id.roundImageView);
                text_wallet  =itemView.findViewById(R.id.text_wallet);
                text_username=itemView.findViewById(R.id.text_username);
                txt_currency =itemView.findViewById(R.id.txt_currency);
                LinearLayout =itemView.findViewById(R.id.LinearLayout);

            }

        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }



    private void setNormalPicker() {

        final Calendar today = Calendar.getInstance();
        findViewById(R.id.month_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(mActivity, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        Log.d(TAG, "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);
                        Toast.makeText(walletActivity.this, "Date set with month" + selectedMonth + " year " + selectedYear, Toast.LENGTH_SHORT).show();
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

                builder.setActivatedMonth(Calendar.JULY)
                        .setMinYear(1990)
                        .setActivatedYear(2017)
                        .setMaxYear(2030)
                        .setMinMonth(Calendar.FEBRUARY)
                        .setTitle("Select trading month")
                        .setMonthRange(Calendar.FEBRUARY, Calendar.NOVEMBER)
                        // .setMaxMonth(Calendar.OCTOBER)
                        // .setYearRange(1890, 1890)
                        // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                        //.showMonthOnly()
                        // .showYearOnly()
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                                Log.d(TAG, "Selected month : " + selectedMonth);
                                // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                                Log.d(TAG, "Selected year : " + selectedYear);
                                // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .build()
                        .show();

            }
        });

    /*    findViewById(R.id.date_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(walletActivity.this, null, 2017,
                        cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
                dialog.show();
            }
        });*/
    }

    private void chooseMonthOnly() {


        findViewById(R.id.month_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(walletActivity.this, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {

                      //  tvLeft.setText((selectedMonth).);
                    }
                }, /* activated number in year */ 3, 5);

                builder.showMonthOnly()
                        .build()
                        .show();
            }
        });
    }

    private void chooseYearOnly(){
        findViewById(R.id.choose_year).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(walletActivity.this, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                     tvRight.setText(Integer.toString(selectedYear));
                      //  choosenYear = selectedYear;
                    }
                }, choosenYear, 0);

                builder.showYearOnly()
                        .setYearRange(1990, 2030)
                        .build()
                        .show();
            }
        });
    }

}
