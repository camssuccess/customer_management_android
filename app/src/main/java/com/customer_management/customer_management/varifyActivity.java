package com.customer_management.customer_management;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.customer_management.customer_management.view.BaseActivity;

public class varifyActivity extends BaseActivity implements View.OnClickListener {
    TextView buttonVarify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.varify_activity);

        init();

    }

    private void init() {
        buttonVarify = findViewById(R.id.buttonVarify);
        onClickListener();
    }
    private void onClickListener() {
        buttonVarify.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonVarify:
                AlertLogoutPopUp();
        }
    }

    private void AlertLogoutPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_activity, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        TextView textView = alertDialog.findViewById(R.id.tvnote3);
        TextView tvnote2 = alertDialog.findViewById(R.id.tvnote2);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(varifyActivity.this,driverComfortActivity.class));

            }
        });
        tvnote2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(varifyActivity.this,driverComfortActivity.class));

            }
        });

    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

}
