package com.customer_management.customer_management;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.customer_management.customer_management.view.BaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class tellUsAboutYouActivity extends BaseActivity implements View.OnClickListener {
      EditText etUrName,etDOB,etMobNo;
    Button btnText;
    String userName="",userdob="",usermobNo="";
    Date date1= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_us_about_you);

        init();
        onClickListener();
    }
    private void init(){
        etUrName=findViewById(R.id.etYourName);
        etDOB=findViewById(R.id.etDOB);
        etMobNo=findViewById(R.id.etMobNo);
        btnText=findViewById(R.id.btnText);
        //   tvSubmit=rootView.findViewById(R.id.tvSubmit);
        onClickListener();
        getText();
    }

    private void getText(){
        userName=etUrName.getText().toString().trim();
        userdob=etDOB.getText().toString().trim();
        usermobNo=etMobNo.getText().toString().trim();
     //   AppUtils.getTncDate(userdob);


       //date formate
        //String sDate1="31/12/1998";

     /*   try {
            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(userdob);
            Toast.makeText(this, ""+date1, Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(userdob+"\t"+date1);*/
    }
    private void onClickListener() {
        btnText.setOnClickListener(this);
        //  tvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnText:

               /* if(etUrName.getText().toString().isEmpty()){
                    Toast.makeText(this, getString(R.string.Checkusername), Toast.LENGTH_SHORT).show();
                }else if(etDOB.getText().toString().isEmpty()){
                    Toast.makeText(this, getString(R.string.Checkuserdob), Toast.LENGTH_SHORT).show();
                }else if(etMobNo.getText().toString().isEmpty()){
                    Toast.makeText(this, getString(R.string.Checkusermobno), Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent=new Intent(this,VarificationActivity.class);
                    startActivity(intent);
                }
*/
              Intent intent=new Intent(this, varificationActivity.class);
              startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}