package com.customer_management.customer_management;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.customer_management.customer_management.view.BaseActivity;

public class callingActivity extends BaseActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;
    ImageView ivUserProfile, ivreceiveCall, ivremoveCall;

    String phone;
    Intent it;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calling_activity);

        if (ContextCompat.checkSelfPermission(callingActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(callingActivity.this,
                    Manifest.permission.CALL_PHONE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(callingActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);

                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
        init();


    }

    private void init() {
        ivUserProfile = findViewById(R.id.ivUserProfile);
        ivremoveCall = findViewById(R.id.ivremoveCall);
        ivreceiveCall = findViewById(R.id.ivreceiveCall);
        ivUserProfile.setOnClickListener(this);
        ivremoveCall.setOnClickListener(this);
        ivreceiveCall.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivUserProfile:
                //StartActivity
                startActivity(new Intent(callingActivity.this, carConfirmationActivity.class));
                break;

            case R.id.ivreceiveCall:
                dialClick();
                break;
            case R.id.ivremoveCall:


                break;
        }
    }

    public void dialClick () {
        phone ="4376821833";
        Uri uri = Uri.parse("tel: " + phone);
        it = new Intent(Intent.ACTION_CALL);
        it.setData(uri);
        startActivity(it);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
