package com.customer_management.customer_management;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.customer_management.customer_management.view.BaseActivity;

public class rideNowActivity extends BaseActivity implements View.OnClickListener {
    TextView tvRideNow;
    LinearLayout lllinear;
    ImageView ivback;
    TextView btnAccept,tvCancel;
    RatingBar ratingBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_now);
        initillization();
    }



    private void initillization(){

        tvRideNow=findViewById(R.id.tvRideNow);
        lllinear=findViewById(R.id.lllinear);
        ivback=findViewById(R.id.ivback);
        btnAccept=findViewById(R.id.btnAccept);
        tvCancel=findViewById(R.id.tvcancel);

        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = "Rating is :" + ratingBar.getRating();
                Toast.makeText(rideNowActivity.this, ""+rating, Toast.LENGTH_LONG).show();
            }
        });

        lllinear.setOnClickListener(this);
        ivback.setOnClickListener(this);
        tvRideNow.setOnClickListener(this);
        btnAccept.setOnClickListener(this);

    }

    public void open(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.map_resource, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        TextView tvPickup = alertDialog.findViewById(R.id.tvPickup);
        TextView tvDropOff = alertDialog.findViewById(R.id.tvdropoff);
        tvPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvDropOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("bug", String.valueOf(view));

            }
        });
       /* final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_resource);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
       */
    }

    @Override
    public void onClick(View view) {
    switch(view.getId()){
    case R.id.tvRideNow:
        open();
        //draweble folder Existing, background image correction,
        //
        break;
    case R.id.ivback:
        onBackPressed();
        break;
    case R.id.btnAccept:
        startActivity(new Intent(this, bookingConfirmedActivity.class));
        break;
}
    }
}
