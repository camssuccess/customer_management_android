package com.customer_management.customer_management;


import androidx.appcompat.app.AlertDialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;


import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.customer_management.customer_management.view.BaseActivity;
import com.squareup.timessquare.CalendarPickerView;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.List;




public class bookingActivity extends BaseActivity implements View.OnClickListener {

    ImageView ivback;
    CalendarView calenderView;
    TimePicker startTime, EndTime;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        initillization();

    }

    private void initillization() {
        ivback = findViewById(R.id.ivback);
        startTime = findViewById(R.id.startTime);
        btnSubmit = findViewById(R.id.btnSubmit);
        final CalendarPickerView calendar_view = findViewById(R.id.calendar_view);
       //getting current
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        Date today = new Date();


        calendar_view.init(today, nextYear.getTime()).inMode(CalendarPickerView.SelectionMode.RANGE);

        ivback.setOnClickListener(this);
        calendar_view.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
              //  Toast.makeText(getApplicationContext(),"Selected Date is : " +date.toString(),Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onDateUnselected(Date date) {

               // Toast.makeText(getApplicationContext(),"UnSelected Date is : " +date.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        btnSubmit.setOnClickListener(this);
        List<Date> dates = calendar_view.getSelectedDates();
       // calenderView.setSelected(true);
       // startTime.setMinimumHeight(90);
       // startTime.setMinimumWidth(90);
        /*calenderView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
             *//*  String msg = "Selected date Day: " + i2 + " Month : " + (i1 + 1) + " Year " + i;
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();*//*
            }
        });*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                onBackPressed();
                break;
            case R.id.btnSubmit:
                AlertLogoutPopUp();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void AlertLogoutPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.fixbookinglayout, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        TextView textView=alertDialog.findViewById(R.id.dvtv);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
       /* final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogueview);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);*/
     //   TextView textView=dialog.findViewById(R.id.dvTextView);
       /* textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

    }
}