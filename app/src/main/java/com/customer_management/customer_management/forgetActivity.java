package com.customer_management.customer_management;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.customer_management.customer_management.view.BaseActivity;

public class forgetActivity extends BaseActivity implements View.OnClickListener {
    EditText etForgetConfirmOTP,etForgetCurrentOTP,etOTP;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_activity);

        findViewById();
    }

    private void findViewById() {
        etForgetConfirmOTP=findViewById(R.id.etForgetConfirmOTP);
        etForgetCurrentOTP=findViewById(R.id.etForgetCurrentOTP);
        etOTP=findViewById(R.id.etForgetOTP);
        btnSubmit=findViewById(R.id.SUBMIT);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.SUBMIT:
                Log.v("DEBug", String.valueOf(view));
                if(etOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "OTP can not be empty", Toast.LENGTH_SHORT).show();
                }else if(etForgetCurrentOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "current otp can not be empty", Toast.LENGTH_SHORT).show();
                }else if(etForgetConfirmOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "confirm otp can not be empty", Toast.LENGTH_SHORT).show();
                }else {
                  startActivity(new Intent(mActivity,dashBoardActivity.class));
                }
                break;
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //on KeyBoard  touch out side event...()
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
