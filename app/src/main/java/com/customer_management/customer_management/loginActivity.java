package com.customer_management.customer_management;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.customer_management.customer_management.view.AppSettings;
import com.customer_management.customer_management.view.BaseActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import java.util.Arrays;


public class loginActivity extends BaseActivity implements View.OnClickListener {
    EditText etUsername, etPassword, etForgotEmail;
    TextView tvForgotPassword;
    RelativeLayout rlRegister;
    Button btnLogin;
    boolean isLogin;
    TextView tvsignup;
    LoginButton loginButton;


    LinearLayout sign_in_google_ll, signup_fb_LL;

    private CallbackManager callbackManager;
    private static final String TAG = "JustFriendz";
    private static final int RC_SIGN_IN = 1001;
    GoogleSignInClient googleSignInClient;
    private FirebaseAuth firebaseAuth;

    private static final String TWITTER_KEY = "YOUR TWITTER KEY";
    private static final String TWITTER_SECRET = "YOUR TWITTER SECRET";

    //Tags to send the username and image url to next activity using intent
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PROFILE_IMAGE_URL = "image_url";

    //Twitter Login Button
    TwitterLoginButton twitterLoginButton;

    String emaildId,password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        configureGoogleClient();

        initillization();
    }

      private void initillization() {

        etUsername = findViewById(R.id.etusername);
        etPassword = findViewById(R.id.etpassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvsignup = findViewById(R.id.tvsignup);
        rlRegister = findViewById(R.id.rlRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
       sign_in_google_ll = findViewById(R.id.sign_in_google_ll);
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

          onClickListener();
      }


    private void onClickListener() {

        btnLogin.setOnClickListener(this);
        tvsignup.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        sign_in_google_ll.setOnClickListener(this);
        rlRegister.setOnClickListener(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvForgotPassword:
                alertPopUP();
               /* startActivity(new Intent(mActivity,forgetActivity.class));*/
                break;
                case R.id.rlRegister:

                moveToSignUp();
                break;

            case R.id.btnLogin:

                moveToLogin();
                break;

            case R.id.sign_in_google_ll:
                googleSignIn();
                break;

            case R.id.signup_fb_LL:
                handleFBLogin();
                break;

        }
    }

    private void moveToLogin() {
        if(etUsername.getText().toString().trim().isEmpty()){
            Toast.makeText(mActivity, "username can not be empty", Toast.LENGTH_SHORT).show();
        }else if(etPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(mActivity, "password can not be empty", Toast.LENGTH_SHORT).show();
        }else {
            if (etUsername.getText().toString().trim().equals("user123") && etPassword.getText().toString().trim().equals("123456")) {
                AppSettings.putString(AppSettings.userName,"user123");
                AppSettings.putString(AppSettings.password,"123456");

                isLogin=true;
                startActivity(new Intent(mActivity, dashBoardActivity.class));
            } else {
                Toast.makeText(mActivity, "please enter valid username and password", Toast.LENGTH_SHORT).show();
            }
        }
    }




    private void googleSignIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            // showToastMessage("Firebase Authentication failed:" + task.getException());
                        }
                    }
                });
    }

    private void configureGoogleClient() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                // for the requestIdToken, this is in the values.xml file that
                // is generated from your google-services.json
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        // Initialize Firebase Auth
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            Log.d(TAG, "Currently Signed in: " + currentUser.getEmail());
            // showToastMessage("Currently Logged in: " + currentUser.getEmail());
        }

    }

    private void handleFBLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn) {
            LoginManager.getInstance().logOut();
            return;
        }

        LoginManager.getInstance().logInWithReadPermissions(loginActivity.this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setFacebookData(loginResult);
                            }
                        });
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(loginActivity.this, "CANCELED", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(loginActivity.this, "ERROR" + exception.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void setFacebookData(final LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                // Application code
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                //  showToastMessage("Google Sign in Succeeded");
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.e(TAG, "Google sign in failed", e);
                // showToastMessage("Google Sign in Failed " + e);
            }
        }
    }



    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        loginActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    private void moveToSignUp(){
        startActivity(new Intent(mActivity,signupActivity.class));
    }

    private void alertPopUP() {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.popup_forgot, viewGroup, false);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        ImageView ivCross = alertDialog.findViewById(R.id.ivCross);
        etForgotEmail = alertDialog.findViewById(R.id.etEmail);

        final TextView tvOkay = alertDialog.findViewById(R.id.tvOkay);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etForgotEmail.getText().toString().isEmpty()) {
                    Toast.makeText(mActivity, "Please enter your contact number", Toast.LENGTH_SHORT).show();
                } else {
                     startActivity(new Intent(mActivity,forgetActivity.class));
                  //  startActivity(new Intent(mActivity,TowTruckActivity.class));
                }
                onResume();
            }
        });





      /*  final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_forgot);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivCross = dialog.findViewById(R.id.ivCross);
        etForgotEmail = dialog.findViewById(R.id.etEmail);

        final TextView tvOkay = dialog.findViewById(R.id.tvOkay);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             dialog.dismiss();
            }
        });
        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etForgotEmail.getText().toString().isEmpty()) {
                    Toast.makeText(dialog.getContext(), "Please enter your contact number", Toast.LENGTH_SHORT).show();
                } else {
                   // startActivity(new Intent(mActivity,forgetActivity.class));
                    startActivity(new Intent(mActivity,TowTruckActivity.class));
                }
                onResume();
            }
        });*/
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }



}